const fs = require('fs').promises;
const {nanoid} = require('nanoid');

const fileName = 'data.json';

let data = [];

module.exports = {
    async init() {
        try {
            const fileContent = await fs.readFile(fileName);
            data = JSON.parse(fileContent);
        } catch (e) {
            data = [];
        }
    },
    async getMessages() {
        return data;
    },
    async addMessage(message) {
        message.id = nanoid();
        data.push(message);
        await this.save();
    },
    async save() {
        await fs.writeFile(fileName, JSON.stringify(data, null, 2));
    }
};