const express = require('express');
const path = require('path');
const multer = require('multer');
const fileWork = require('../fileWork');
const {nanoid} = require('nanoid');
const config = require('../config');
const router = express.Router();

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

router.post('/', upload.single('image'), async (req, res) => {
    const message = req.body;

    if (req.file) {
        message.image = req.file.filename;
    }

    if (message.image === "undefined") {
        message.image = '';
    }

    if (!message.name) {
        message.name = 'anonymous';
    }

    await fileWork.addMessage(message);
    res.send(message);
});

router.get('/', async (req, res) => {
    const messages = await fileWork.getMessages();
    res.send(messages);
});


module.exports = router;