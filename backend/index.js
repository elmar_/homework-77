const express = require('express');
const cors = require('cors');
const messages = require('./app/messages');
const fileWork = require('./fileWork');

const app = express();
app.use(express.json());
app.use(express.static('public'));
app.use(cors());

const port = 8000;
app.use('/messages', messages);

const run = async () => {

    await fileWork.init();

    app.listen(port, () => {
        console.log('We are on port ' + port);
    });
};

run().catch(console.error);