import React, {useEffect} from 'react';
import Container from "@material-ui/core/Container";
import {useDispatch, useSelector} from "react-redux";
import FormBlock from "../../components/FormBlock/FormBlock";
import {fetchData, postMessage} from "../../store/actions";
import {makeStyles, Paper} from "@material-ui/core";
import Message from "../../components/Message/Message";
import {Alert} from "@material-ui/lab";

const useStyles = makeStyles((theme) => ({
    paper: {
        backgroundColor: '#f5f5f5',
        marginBottom: theme.spacing(4)
    }
}));

const Main = () => {
    const classes = useStyles();
    const dispatch = useDispatch();
    const data = useSelector(state => state.data);
    const postError = useSelector(state => state.postError);
    const fetchError = useSelector(state => state.errorFetch);

    let error = null;

    if (postError) {
        error = <Alert severity="error">Error. Your message was not send. Try again</Alert>
    }

    if (fetchError) {
        error = <Alert severity="error">Error. Can not get data from server</Alert>
    }

    useEffect(() => {
        const interval = setInterval(() => {
            dispatch(fetchData());
        }, 3000);

        return () => {
            clearInterval(interval);
        };
    });

    const onSubmit = async message => {
        dispatch(postMessage(message));
    };


    return (
        <Container>
            {error}
            <Paper levation={3} variant="outlined" className={classes.paper}>
                {data.map(mes => (
                    <Message
                        name={mes.name}
                        id={mes.name}
                        message={mes.message}
                        image={mes.image}
                        key={mes.id}
                    />
                ))}
            </Paper>
            <FormBlock onSubmit={onSubmit} />
        </Container>
    );
};

export default Main;