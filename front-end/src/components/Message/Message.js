import React from 'react';
import './Message.css';
import {apiURL} from "../../apiURL";

const Message = ({image, name, message}) => {
    let img = null;
    if (image) {
        img = <img src={apiURL + '/uploads/' + image} alt='#' className="img" />;
    }

    return (
        <div className="Message">
            <h3>{name}:</h3>
            {img}
            <p>{message}</p>
        </div>
    );
};

export default Message;