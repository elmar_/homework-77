import React, {useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import FileInput from "../FileInput/FileInput";
import Button from "@material-ui/core/Button";


const FormBlock = ({onSubmit}) => {

    const [state, setState] = useState({
        name: '',
        message: '',
        image: ''
    });

    const onChangeForm = e => {
        const {name, value} = e.target;

        setState(prevState => ({
            ...prevState,
            [name]: value
        }));
    };

    const fileChangeHandler = e => {
        const name = e.target.name;
        const file = e.target.files[0];

        setState(prevState => ({
            ...prevState,
            [name]: file
        }));
    };

    const onSubmitHandler = e => {
        e.preventDefault();

        const formData = new FormData();

        Object.keys(state).forEach(key => {
            formData.append(key, state[key]);
        });

        onSubmit(formData);
    };


    return (
        <form onSubmit={onSubmitHandler}>
            <Grid container spacing={2} direction="column">
                <Grid item xs>
                    <TextField
                        fullWidth
                        label="Name"
                        variant="outlined"
                        name="name"
                        value={state.name}
                        onChange={onChangeForm}
                    />
                </Grid>
                <Grid item xs>
                    <FileInput
                        onChange={fileChangeHandler}
                        name="image"
                        label="Image"
                    />
                </Grid>
                <Grid item xs>
                    <TextField
                        required
                        fullWidth
                        label="Message"
                        variant="outlined"
                        name="message"
                        multiline
                        rows={3}
                        value={state.message}
                        onChange={onChangeForm}
                    />
                </Grid>
                <Grid item xs>
                    <Button type="submit" color="primary" variant="contained">Send</Button>
                </Grid>
            </Grid>
        </form>
    );
};

export default FormBlock;