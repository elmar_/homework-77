import React, {useRef, useState} from 'react';
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles({
    input: {
        display: 'none'
    }
});

const FileInput = ({onChange, name, label}) => {
    const classes = useStyles();
    const inputRef = useRef();

    const [fileName, setFileName] = useState('');

    const activateInput = () => {
        inputRef.current.click();
    };

    const onFileChange = e => {
        if (e.target.files[0]) {
            setFileName(e.target.files[0].name);
        } else {
            setFileName('');
        }

        onChange(e);
    };

    return (
        <>
            <input
                type="file"
                name={name}
                className={classes.input}
                ref={inputRef}
                onChange={onFileChange}
            />
            <Grid container>
                <Grid item xs>
                    <TextField
                        variant="outlined"
                        disabled
                        fullWidth
                        label={label}
                        onClick={activateInput}
                        value={fileName}
                    />
                </Grid>
                <Grid item>
                    <Button variant="contained" onClick={activateInput}>Browser</Button>
                </Grid>
            </Grid>
        </>
    );
};

export default FileInput;