import React from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles(theme => ({
    staticToolBar: {
        marginBottom: theme.spacing(3)
    }
}));

const Header = () => {
    const styles = useStyles();
    return (
        <>
            <CssBaseline />
                <AppBar>
                    <Toolbar>
                        <Typography variant="h6">Анонимный Форум</Typography>
                    </Toolbar>
                </AppBar>
            <Toolbar className={styles.staticToolBar} />
        </>
    );
};

export default Header;