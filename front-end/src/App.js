import React from 'react';
import Header from "./components/Header/Header";
import Main from "./containers/Main/Main";

const App = () => {
    return (
        <>
         <Header />
         <Main />
        </>
    );
};

export default App;