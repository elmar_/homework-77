import {FETCH_DATA_ERROR, FETCH_DATA_SUCCESS, POST_MESSAGE_ERROR, POST_MESSAGE_SUCCESS} from "./actions";

const initialState = {
    data: [],
    errorFetch: false,
    postError: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case FETCH_DATA_SUCCESS:
            return {...state, data: action.data, errorFetch: false};
        case FETCH_DATA_ERROR:
            return {...state, errorFetch: true};
        case POST_MESSAGE_ERROR:
            return {...state, postError: true};
        case POST_MESSAGE_SUCCESS:
            return {...state, postError: false}
        default:
            return state;
    }
};

export default reducer;