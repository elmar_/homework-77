import axiosFetch from "../axios-fetch";


export const FETCH_DATA_SUCCESS = 'FETCH_DATA_SUCCESS';
export const FETCH_DATA_ERROR = 'FETCH_DATA_ERROR';

export const fetchDataSuccess = data => ({type: FETCH_DATA_SUCCESS, data});
export const fetchDataError = () => ({type: FETCH_DATA_ERROR});

export const POST_MESSAGE_SUCCESS = 'POST_MESSAGE_SUCCESS';
export const POST_MESSAGE_ERROR = 'POST_MESSAGE_ERROR';

export const postMessageSuccess = () => ({type: POST_MESSAGE_SUCCESS});
export const postMessageError = () => ({type: POST_MESSAGE_ERROR});

export const postMessage = message => {
    return async dispatch => {
        try {
            await axiosFetch.post('/messages', message);
            dispatch(postMessageSuccess());
        } catch (e) {
            dispatch(postMessageError());
        }
    };
};

export const fetchData = () => {
    return async dispatch => {
        try {
            const response = await axiosFetch.get('/messages');
            dispatch(fetchDataSuccess(response.data));
        } catch (e) {
            dispatch(fetchDataError());
        }
    };
};

