import axios from "axios";
import {apiURL} from "./apiURL";

const axiosFetch = axios.create({
    baseURL: apiURL
});

export default axiosFetch;